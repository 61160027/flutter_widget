import 'package:flutter/material.dart';

class MyScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return Text('Hello, World!');
  }

}

void main() {
  runApp(
    MaterialApp(
      title: 'My app',
      home: SafeArea(
        child: MyScaffold(),
        )
    )
  );
}